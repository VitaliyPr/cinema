document.addEventListener('DOMContentLoaded', () => {
    AOS.init();
    $('select').niceSelect();

    $('html').imagesLoaded({
      background: true
    }, function () {
      $('body').removeClass('loading');
    });
});

// Меню
function openNav() {
  if ($(window).width() < 767.98) {
    document.getElementById('mySidepanel').style.width = '100%';
  } else {
    document.getElementById('mySidepanel').style.width = '100vw';
  };
};

function closeNav() {
  document.getElementById('mySidepanel').style.width = '0';
};


function pageWidth() {
    return window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
}

// if (document.querySelector('.')){
//   let slider = new Swiper('.', {
    
//    observer: true,
//    observeParents: true,
//    slidesPerView: 1,
//    spaceBetween: 0,
//    autoHeight: true,
//    speed: 800,
//     // touchRatio: 0,
//     // simulateTouch: false,
//     // loop: true,
//     // preloadImages: false,
//     // lazy: true,
//     // parallax: true,
//     /*
//     pagination: {
//       el: '.controls-slider-main__dotts',
//       clickable: true,
//     },
//     */
//     /*
//     navigation: {
//       nextEl: '.slider-main .slider-arrow_next',
//       prevEl: '.slider-main .slider-arrow_prev',
//     }
//     */
//   });
// };