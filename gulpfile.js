// https://www.youtube.com/watch?v=fhfx4s9AbBI&t=234s
// https://webdesign-master.ru/blog/html-css/2019-05-08-css-gallery.html

var 	gulpVersion    = '4'; // Gulp version: 3 or 4
		gmWatch        = true; // ON/OFF GraphicsMagick watching "img/_src" folder (true/false). Linux install gm: sudo apt update; sudo apt install graphicsmagick

var 	gulp          = require('gulp'),
		babel 		  = require('gulp-babel'),
		browserSync   = require('browser-sync'),
		imageResize   = require('gulp-image-resize'),
		imagemin      = require('gulp-imagemin'),
		del           = require('del'),
		cleancss      = require('gulp-clean-css'),
		rename        = require('gulp-rename'),
		tiny 		  = require('gulp-tinypng-compress'),
		concat        = require('gulp-concat'),
		autoprefixer  = require('gulp-autoprefixer'),
		sass          = require('gulp-sass'),
		uglify        = require('gulp-uglify'),
		notify        = require('gulp-notify'),
		fileinclude   = require('gulp-file-include'),
		ttf2woff2     = require('gulp-ttf2woff2'),
		ttf2woff      = require('gulp-ttf2woff'),
		gcmq          = require("gulp-group-css-media-queries"),
        svgSprite     = require('gulp-svg-sprite'),
        svgmin        = require('gulp-svgmin'),
        cheerio       = require('gulp-cheerio'),
        replace       = require('gulp-replace');



// CLEAN DIST

gulp.task('cleanDist', function() {
	return del(['dist'], { force: true })
});


// FONTS


gulp.task('fontsToWoff2', function(){
  return gulp.src(['src/fonts/**/*.ttf'])
    .pipe(ttf2woff2())
    .pipe(gulp.dest('dist/fonts/'));
});

gulp.task('fontsToWoff', function(){
  return gulp.src(['src/fonts/**/*.ttf'])
    .pipe(ttf2woff())
    .pipe(gulp.dest('dist/fonts/'));
});

gulp.task('fonts', gulp.parallel('fontsToWoff2', 'fontsToWoff'));


// Local Server

gulp.task('browser-sync', function() {
	browserSync({
		server: {
			baseDir: 'dist'
		},
		notify: false,
		open: false,
		// online: false, // Work Offline Without Internet Connection
		// tunnel: true, tunnel: "projectname", // Demonstration page: http://projectname.localtunnel.me
	})
});

// Styles

gulp.task('concatCSSPluginsWatch', function() {
	return gulp.src('src/styles/plugins/**/*.css')
	.pipe(concat('plugins.min.css'))
	.pipe(gulp.dest('dist/css/plugins/'))
});

gulp.task('cssWatch', function() {
	return gulp.src(['src/styles/style.scss'], {base: "src/styles/"})
	.pipe(sass({ outputStyle: 'expanded' }).on("error", notify.onError()))
	.pipe(gulp.dest('dist/css/'))
	.pipe(browserSync.stream());
});


gulp.task('concatCSSPluginsBuild', function() {
	return gulp.src('src/styles/plugins/**/*.css')
	.pipe(concat('plugins.min.css'))
	.pipe(autoprefixer(['last 15 versions']))
	.pipe(cleancss())
	.pipe(gulp.dest('dist/css/plugins/'))
});




gulp.task('cssBuild', function() {
	return gulp.src(['src/styles/style.scss'])
	.pipe(sass({ outputStyle: 'expanded' }).on("error", notify.onError()))
	.pipe(autoprefixer(['last 15 versions']))
	.pipe(cleancss({
      level: {
		2: {
			mergeAdjacentRules: false
		}
	  },
	  format: 'beautify'
    }))
	.pipe(gcmq())
	.pipe(gulp.dest('dist/css/'))
});



// JS
gulp.task('concatJSPluginsWatch', function() {
    return gulp.src(['src/js/plugins/**/*.js'])
        .pipe(concat('plugins.min.js'))
        .pipe(gulp.dest('dist/js/plugins/'));
});

gulp.task('jsWatch', function() {
	return gulp.src('src/js/app.js')
	.pipe(babel({
      presets: ['@babel/env']
    }))
	.pipe(gulp.dest('dist/js/'))
	.pipe(browserSync.reload({ stream: true }))
});


gulp.task('concatJSPluginsBuild', function() {
    return gulp.src([
		'src/js/plugins/**/*.js'
		])
        .pipe(concat('plugins.min.js'))
		.pipe(uglify({
			output: {
				comments: 'all'
			}
		}).on("error", notify.onError()))
        .pipe(gulp.dest('dist/js/plugins/'));
});

gulp.task('jsBuild', function() {
	return gulp.src('src/js/app.js')
	.pipe(babel({
      presets: ['@babel/env']
    }))
	.pipe(gulp.dest('dist/js/'))
});






// HTML



gulp.task('htmlInclude', function() {
  return gulp.src(['src/*.html'])
    .pipe(fileinclude({
      prefix: '@',
      basepath: '@file'
    }))
    .pipe(gulp.dest('dist'))
	.pipe(browserSync.stream());
});

gulp.task('code', function() {
	return gulp.src('src/*.html')
	.pipe(browserSync.reload({ stream: true }))
});

gulp.task('htmlBuild', function() {
  return gulp.src(['src/*.html'])
    .pipe(fileinclude({
      prefix: '@',
      basepath: '@file'
    }))
    .pipe(gulp.dest('dist'))
});



// IMG


gulp.task('imgWatch', function() {
	return gulp.src(['src/img/**/*.{png,jpg,jpeg,gif,svg}'])
	.pipe(gulp.dest('dist/img/'))
});


gulp.task('svg', function () {
	return gulp.src('src/img/sprite/**.svg')
		.pipe(svgmin({
			js2svg: {
				pretty: true
			}
		}))
		.pipe(cheerio({
			run: function ($) {
				$('[fill]').removeAttr('fill');
				$('[stroke]').removeAttr('stroke');
				$('[style]').removeAttr('style');
			},
			parserOptions: { xmlMode: true }
		}))
		.pipe(replace('&gt;', '>'))
		.pipe(svgSprite({
			mode: {
				symbol: {
					sprite: '../sprite.svg',
					render: {
						scss: {
							dest: '../../styles/_sprite.scss',
							template: 'src/styles/utils/_sprite_template.scss'
						}
					}
				},

			}
		}))
		.pipe(gulp.dest('src/img/'));
});


// Tinypng 


gulp.task('tinypng', function () {
    gulp.src('src/img/**/*.{png,jpg,jpeg}')
    .pipe(tiny({
      key: 'JfQSX99kJnFCXnhbNBqnF6Q3LkQ8nGNW',
      sigFile: 'src/img/.tinypng-sigs',
      parallel: true,
      parallelMax: 50,
      log: true,
    }))
    .pipe(gulp.dest('dist/img/'))
});

gulp.task('imgCompress', function() {
	return gulp.src('src/img/**/*.{png,jpg,jpeg}')
	.pipe(imagemin())
	.pipe(gulp.dest('dist/img/'))
});

gulp.task('img1x', function() {
	return gulp.src('src/img/**/*.{png,jpg,jpeg}')
	.pipe(imageResize({ width: '50%' }))
	.pipe(imagemin())
	.pipe(gulp.dest('dist/img/@1x/'))
});

gulp.task('img2x', function() {
	return gulp.src('src/img/**/*.{png,jpg,jpeg}')
	.pipe(imageResize({ width: '100%' }))
	.pipe(imagemin())
	.pipe(gulp.dest('dist/img/@2x/'))
});


gulp.task('imgCopy', function() {
	return gulp.src('src/img/**/*.{webp,gif,svg}')
	.pipe(gulp.dest('dist/img/'))
});

gulp.task('imgBuild', gulp.parallel('imgCompress', 'imgCopy'));


// WATCH

gulp.task('watch', function() {
	gulp.watch(['src/styles/plugins/**/*.css', 'src/styles/**/*.scss'], gulp.parallel('concatCSSPluginsWatch', 'cssWatch'));
	gulp.watch(['src/js/plugins/**/*.js', 'src/js/app.js'], gulp.parallel('concatJSPluginsWatch', 'jsWatch'));
	gulp.watch(['src/img/**/*.{png,jpg,jpeg,gif,svg}'], gulp.parallel('imgWatch')); 
    gulp.watch(['src/img/sprite/**.svg'], gulp.parallel('svg')); 
	gulp.watch(['src/**/*.html'], gulp.parallel('htmlInclude')); 
	
});




gulp.task('default', gulp.series('cleanDist', 'fontsToWoff2', 'svg', gulp.parallel('concatCSSPluginsWatch', 'cssWatch', 'concatJSPluginsWatch', 'jsWatch', 'htmlInclude', 'imgWatch', 'browser-sync', 'watch')));



gulp.task('prebuild', gulp.series('cssBuild', 'jsBuild', 'htmlBuild'));

gulp.task('build', gulp.series('cleanDist', 'fontsToWoff2', 'svg', 'concatCSSPluginsBuild', 'cssBuild', 'concatJSPluginsBuild', 'jsBuild', 'htmlBuild', 'imgWatch'));
